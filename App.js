import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { useEffect, useState } from "react";

export default function App() {
  const [count, setCount] = useState(0);

  const [dado, setDado]  = useState(0);
  function rodarDado(){
    setDado(Math.floor(Math.random() * 6 ) +1);
  }

  return (
    <View style={styles.container}>
      <ScrollView>
        {/* <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text>
        <Text>Cade o mundial</Text> */}
        <Text style={styles.fonts}>
          Números de homem que o igor já beijou ( {count})
        </Text>
        <TouchableOpacity
          style={styles.teste}
          onPress={() => setCount(count + 9999)}
        >
          <Text>Clique</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style = {styles.teste}
          onPress={() => rodarDado()}
        >
          <Text>Jogue o dado!</Text>
          
        </TouchableOpacity>
        <Text>Número Sorteado:{dado}</Text>
      </ScrollView>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "pink",
    alignItems: "center",
    justifyContent: "center",
  },

  teste: {
    color: "black",
    backgroundColor: "red",
    width: 90,
    borderRadius: 25,
    alignItems: "center",
  },

  fonts: {
    fontSize: 24,
    fontWeight: "bold",
  },

  botao: {
    width: 200,
    height: 200,
  },
});
